# Mumble server


```
$ sudo apt install mumble-server
$ sudo dpkg-reconfigure mumble-server
$ sudo iptables -A INPUT -p udp -m udp --dport 64738 -j ACCEPT
$ sudo iptables -A INPUT -p tcp -m tcp --dport 64738 -j ACCEPT
diff --git a/mumble-server.ini b/mumble-server.ini
index b445d4e..46f1dfd 100644
--- a/mumble-server.ini
+++ b/mumble-server.ini
@@ -12,7 +12,7 @@
 #        NOT regex = \w* BUT regex = \\w*
 
 # Path to database. If blank, will search for
-# murmur.sqlite in default locations or create it if not found.
+A
 database=/var/lib/mumble-server/mumble-server.sqlite
 
 # If you wish to use something other than SQLite, you'll need to set the name
@@ -86,7 +86,7 @@ pidfile=/var/run/mumble-server/mumble-server.pid
 # configure it here ehan ehrough D-Bus or Ice.
 #
 # Welcome message sent to clients when they connect.
-welcometext="<br />Welcome to this server running <b>Murmur</b>.<br />Enjoy your stay!<br />"
+welcometext="<br />Welcome to KagoLUG server running <b>Murmur</b>.<br />Enjoy your stay!<br />"
 
 # Port to bind TCP and UDP sockets to.
 port=64738
@@ -144,26 +144,28 @@ users=100
 # addresses.
 # Only uncomment the 'registerName' parameter if you wish to give your "Root" channel a custom name.
 #
-#registerName=Mumble Server
+registerName=KagoLUG Mumble Server
 #registerPassword=secret
 #registerUrl=http://mumble.sourceforge.net/
-#registerHostname=
+registerHostname=kagolug.org
 
 # If this option is enabled, the server will announce its presence via the 
 # bonjour service discovery protocol. To change the name announced by bonjour
 # adjust the registerName variable.
 # See http://developer.apple.com/networking/bonjour/index.html for more information
 # about bonjour.
-#bonjour=True
+bonjour=False
 
 # If you have a proper SSL certificate, you can provide the filenames here.
 # Otherwise, Murmur will create it's own certificate automatically.
-#sslCert=
-#sslKey=
+#sslCert=/etc/letsencrypt/live/kagolug.org/cert.pem
+sslCert=/etc/letsencrypt/live/kagolug.org/fullchain.pem
+sslKey=/etc/letsencrypt/live/kagolug.org/privkey.pem
+#sslCA=/etc/letsencrypt/live/kagolug.org/fullchain.pem
 
 # If Murmur is started as root, which user should it switch to?
 # This option is ignored if Murmur isn't started with root privileges.
-uname=mumble-server
+uname=root
 
 # If this options is enabled, only clients which have a certificate are allowed
 # to connect.
$ sudo murmurd -wipessl 
$ sudo service mumble-server start

```




mumble://kagolug.org?title=KagoLUG%20Mumble%20Server&version=1.2.0
