<!-- page_number: true -->
<!-- $size: 4:3 -->
<!-- footer: @matoken -->


# ゲーミングプラットホームの**Lutris**紹介
### 鹿児島Linux勉強会2017.01(2017-01-04)
### サンエールかごしま 中研修室1
###

##### KenichiroMATOHARA([@matoken](https://twitter.com/matoken/))
##### http://matoken.org 

---

# Linuxでゲーム!

---

# [Lutris?](https://lutris.net/)

[![](./lutris-logo.png)https://lutris.net/](https://lutris.net/)

オープンゲーミングプラットホーム
各種ゲーム環境を一元管理出来る
- Linux
- Steam(Linux/Win)
- Wine
- ゲームエンジン
- 各種エミュレータ等々(著作権等に気をつけて)

---

<!-- bg:./lutris-logo.png -->

# 対応プラットホーム

```
Native Linux
Windows
Steam (Linux and Windows)
MS-DOS
Arcade machines
Amiga computers
Atari 8 and 16bit computers and consoles
Browsers (Flash or HTML5 games)
Commmodore 8 bit computers
SCUMM based games and other point and click adventure games
Magnavox Odyssey², Videopac+
Mattel Intellivision
NEC PC-Engine Turbographx 16, Supergraphx, PC-FX
Nintendo NES, SNES, Game Boy, Game Boy Advance
Game Cube and Wii
Sega Master Sytem, Game Gear, Genesis, Dreamcast
SNK Neo Geo, Neo Geo Pocket
Sony PlayStation
Sony PlayStation 2
Sony PSP
Z-Machine games like Zork
```
---

# Steam

- PCゲーミングプラットホーム(Windows/Mac/Linux)
- ゲームの購入，管理は勿論レビュー，SNS，キュレーション等々様々な機能がある
- SteamOS(Ubuntuベース)というものも
- ゲームにより対応環境はまちまち
  - やっぱりWindows対応が一番多い
  - 最近はVR対応ゲームも(まだWindowsのみだけど)
  - Linux対応ゲームはだんだん増えていっている
  - 日本のゲームも増えてる(同人ゲームとかも，おま国……うｎ)
- よくセールやってるので気になるゲームはウィッシュリストに入れておくとメールで安売りを教えてくれるのでおすすめ
- といっても結構お安い(無料や100円未満から色々)

---

- SteamアイコンがLinux対応
![](steam-os.jpg)
- システム要件
![](steam-sys.jpg)

---

- VR対応
![](steam-vr.jpg)

- WORKSHOP
![](steam-workshop.jpg)

---

-  スレッド スクリーンショット 作品 ブロードキャスト ムービー ワークショップ ニュース ガイド レビュー
- ゲーム毎にこれらが用意されている
![](unturned.jpg)

---

# セール
- ホリデーセールとかよくある
  (そういえば昨日の03:00迄ウィンターセールだった)
- 次は例年だと旧正月あたり
  - セール期間関係なくセールされることもあるのでウィッシュリストに入れておくといい

---

# Wine Steam

- WineはWindows APIの実装でUnix環境でWindowsアプリケーションを動作させることが出来る
- 動くアプリもあれば動かないアプリも(設定変更で動いたりも)
  - 動く動かないはWineHQ AppDBが参考になる -> https://appdb.winehq.org/
- ゲームに関してはLitrusでお手軽に導入&管理が出来る
  - その中にWindows用Steamを動かすWine Steamがある
    Wineが必要->Wineが自動的に導入される
    Monoが必要->Monoが自動的に導入される
    :

---

# LutrisでWine Steamを使う

- 各種ディストリビューション向けの導入方法が公式にある
https://lutris.net/downloads/
- Lutrisを起動して[Lutris]->[Manage runners]から`Wine Steam` -> Installで導入
- 日本語などは文字化けするので日本語フォントを導入しておく(入れないと日本語が豆腐になったり英語のゲームでもフォントが空欄になったりする)
  - Winetricksで導入する例
```shell
$ WINEPREFIX=${HOME}/.local/share/lutris/runners/winesteam/prefix winetricks allfonts
```
- 後は普通にWindows用Steamが使える

---

# LutrisでWine Steamを使う

- Lutrisのページでアカウントを取得してSteamと連携&Lutrisで同期しておくと便利
- ゲームタイトルから環境の導入も出来る(こっちのほうが簡単?)
  - Windows Steam環境のゲームを選ぶとWine Steamが導入されてからそのタイトルの導入になる


---

# [Steamの返金システム](http://store.steampowered.com/steam_refunds/?l=japanese)

- 買ってWineでテストして動かなかったら返品という手もありかも

> Steamストアで2週間以内に購入され、使用時間が2時間未満のゲーム或いはソフトウェアアプリケーションに対して返金を行います

- とはいえ程々に

> 濫用
返金は、Steamでのご購入からリスクを取り除く為に設計されており、ゲームを無料で試すためのシステムではありません。返金システムを濫用していると弊社が判断した場合、 返金リクエストにお応えできない場合があります。

---

# Lutris動作確認済ゲーム

- Lutrisのページに動作確認済ゲームが確認できる
https://lutris.net/games/
- 載っていないゲームも結構動作するので試そう
- 動作したら登録しよう!
https://lutris.net/games/add-game/

---

# Montaro動いたし登録した

![](lutris-montaro.jpg)

---

- Lutrisのおかげで最近はWin用か……．と諦めるパターンより
- Win用かでもLutrisによると動くらしい!スペック足りない……．となって諦めるパターンが増えました
  - 手持ちがオンボードGPUのみというのが大きいのですがorz
  - （10xx欲しい)

---

powered by [Marp - Markdown Presentation Writer](https://yhatt.github.io/marp/ "Marp - Markdown Presentation Writer")
Licence : [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/)