<!-- page_number: true -->
<!-- $size: 4:3 -->
<!-- footer: @matoken -->


# Raspberry PiのUSB OTGを試す

##### KenichiroMATOHARA([@matoken](https://twitter.com/matoken/))
##### http://matoken.org 
### 鹿児島Linux勉強会2017.01(2017-01-04)
### サンエールかごしま 中研修室1

---

# [KenichiroMATOHARA](http://matoken.org)( [@matoken](https://twitter.com/matoken) )
  * PC-UNIX/OSS, OpenStreetMap, 電子工作, 自転車……

![](osmo.jpg)

---

# Raspberry PiのUSB OTGを試す

---

# Raspberry Pi

- 英国製のちっこいコンピュータ
- 幾つか種類がある
- 今回のOTGが利用できるのはA/A+/ZeroでBシリーズでは利用できない
  - A/A+はUSB A-Aになるのでケーブルの入手性が悪い
  - Zeroは一般的なUSB A-microUSB
    - でも本体の入手性が悪い
    - 今のところ海外通販，今年日本国内代理店販売されるはず

---

# いろいろなPi

![](Pi-Family-Photo-Master-Nov-2016_1500.jpg)

---

# Raspberry Pi ZERO
![](pizero.jpg)

---

# USB OTG

- USB On-The-Go
- USB ホストとしてもデバイスとしても利用できる
- AndroidをPCに繋ぐとMTPデバイスとして見えるけど，AndroidにUSBキーボードを繋ぐとUSBホストとして動作して利用できるなど
- Raspberry Piでもこれが利用できる
  - 似たものでBBBや96board等動作するものは多い
- 利用内容にもよるけどmicroUSBケーブル1本だけ(電源兼通信)で利用出来るので便利

---

# Raspberry Piで利用できそうなプロトコル

[Simple guide for setting up OTG modes on the Raspberry Pi Zero](https://gist.github.com/gbaman/50b6cca61dd1c3f88f41)
- シリアル (g_serial)
- イーサネット (g_ether)
- マスストレージ (g_mass_storage)
- MIDI (g_midi)
- オーディオ (g_audio)
- キーボード，マウス，ジョイスティック (g_hid)
- マスストレージ&シリアル (g_acm_ms)
- イーサネット&シリアル (g_cdc)
- イーサネット&マスストレージ&シリアル (g_multi)
- ウェブカメラ (g_webcam)
- プリンター (g_printer)
- テスト用? (g_zero)

---

# 他にもありそう?
```
$ find /lib//modules/`uname -r`/ -name "g_cdc.ko" | sed -e s/g_cdc.ko// | xargs ls -A
g_acm_ms.ko  g_ether.ko         g_midi.ko     g_serial.ko  gadgetfs.ko
g_audio.ko   g_hid.ko           g_multi.ko    g_webcam.ko
g_cdc.ko     g_mass_storage.ko  g_printer.ko  g_zero.ko
```
---

# USB OTGを利用するためのRaspberry Pi(Raspbian)での前準備

- パスワードを規定値から変更
` $ passwd`
- OSを最新にする
`$ sudo apt update && sudo apt upgrade -y`
- firmwareを最新にする(Raspbian 2016-05-10以降は要らない?)
`sudo rpi-update`
- dwc2 USB driverが利用できるように`/boot/config.txt`を編集
```bash
$ echo "# USB OTG
dtoverlay=dwc2" | sudo tee -a /boot/config.txt
```
- 再起動で反映

---

# シリアルを使ってみる(Raspberry Pi認識)

- Raspberry Pi側でシリアルの`g_serial` moduleを読み込む
```
$ sudo modprobe g_serial
$ lsmod|grep g_serial
g_serial                3753  0 
libcomposite           49383  3 usb_f_acm,usb_f_rndis,g_serial
$ dmesg | tail -8
[  332.804810] g_serial gadget: Gadget Serial v2.4
[  332.804840] g_serial gadget: g_serial ready
[  332.804887] dwc2 20980000.usb: dwc2_hsotg_enqueue_setup: failed queue (-11)
[  332.807914] dwc2 20980000.usb: bound driver g_serial
[  333.013000] dwc2 20980000.usb: new device is high-speed
[  333.093343] dwc2 20980000.usb: new device is high-speed
[  333.161825] dwc2 20980000.usb: new address 26
[  335.546058] g_serial gadget: high-speed config #2: CDC ACM config
```
- `/dev/ttyGS0`が生えてくる
```
$ ls -ltr /dev/tty*| tail -2
crw-rw---- 1 root dialout 243,  0 12月 24 18:24 /dev/ttyGS0
crw------- 1 pi   tty     204, 64 12月 24 18:33 /dev/ttyAMA0
```

---

# シリアルを使ってみる(PC認識)

- PC側での認識例(/dev/ttyACM0として認識)
```
[137739.592791] usb 4-1.2: new high-speed USB device number 26 using ehci-pci
[137739.701258] usb 4-1.2: New USB device found, idVendor=0525, idProduct=a4a7
[137739.701264] usb 4-1.2: New USB device strings: Mfr=1, Product=2, SerialNumber=0
[137739.701268] usb 4-1.2: Product: Gadget Serial v2.4
[137739.701271] usb 4-1.2: Manufacturer: Linux 4.4.34+ with 20980000.usb
[137739.701419] usb 4-1.2: Device is not authorized for usage
[137742.073829] cdc_acm 4-1.2:2.0: ttyACM0: USB ACM device
[137742.074747] usbcore: registered new interface driver cdc_acm
[137742.074748] cdc_acm: USB Abstract Control Model driver for USB modems and ISDN adapters
```

---

# シリアルを使ってみる(叩いてみる)

- Raspberry Pi側で待ち受け
```
$ cat /dev/ttyGS0
```
- PC側からメッセージを送ってみる
```
$ echo "hello pi." > /dev/ttyACM0
```
- Raspberry Pi側にメッセージが
```
hello pi.
```

---

# シリアルモジュールのアンロード

```
$ sudo modprobe -r g_serial
$ lsmod | grep g_serial
```

---

# シリアルモジュールの自動読み込み設定

- kernelパラメータに書く方法
`/boot/cmdline.txt`を開いて`rootwait`の後ろに`modules-load=dwc2,g_serial`を追記する

- /etc/modulesに書く方法
```
$ echo "g_serial" | sudo tee -a /etc/modules
```
- どちらかの手順でOK
- 再起動で反映

---

# シリアルをgettyに繋いでみる

```
$ sudo systemctl enable serial-getty@ttyGS0.service
```
- これでホストから繋ぐとコンソールが利用できる
```
$ cu -l /dev/ttyACM0 -s 115200
Connected.

Raspbian GNU/Linux 8 pizero ttyGS0

pizero login: 
```
※モジュールがロードされるのが遅いのでGPIOのシリアルと違いブートメッセージを見ることは出来ない(と思う)

---

# 利用例

- シリアルコンソールでホストから繋いでメンテナンス(GPIOより配線が少ない)
- GPSモジュールの結果を出力してUSB GPSに
- [bcm2708-rngを利用して乱数生成器に](https://plus.google.com/u/0/+KenichiroMATOHARA/posts/jBdT9CCi6cS)
 :

---

# イーサネットを使ってみる

- `g_ether`を読み込むと`/dev/usb0`が生えてくる
- あとは普通のネットワークカードとして設定すればOK
- HOST PCで「他のコンピューターへ共有」と設定すると便利
![](networkmanager-share.jpg)

---

# シリアルとイーサネットを同時に利用する

- `g_cdc`を読み込むとシリアルとイーサネットが同時に利用できる
- `/dev/ttyGS0`と`/dev/usb0`が生えてくる
- 日常使いに便利

---

# マスストレージを利用する

- `g_mass_storage`に引数を付けて読み込むとマスストレージ(USBメモリ)として利用できる
- 引数にはデバイスやファイルが指定できる
  - `/dev/mmcblk0p3`
  - `/home/pi/disk.img`
  ファイルの前準備例

```
$ dd if=/dev/zero of=/home/pi/disk.img bs=1M count=1024
$ /sbin/mkfs.vfat /home/pi/disk.img
```

- 同時に複数指定も出来る

```
$ sudo modprobe g_mass_storage file=/dev/mmcblk0p3,/home/pi/disk.img
```

---

# イーサネット&マスストレージ&シリアルを同時に利用する

- `g_multi`を読み込むと同時に利用できる

---

# キーボード，マウス，ジョイスティック

- HID(g_hid)で行けそうなんだけど……
```
pi@pizero:~$ sudo modprobe g_hid
modprobe: ERROR: could not insert 'g_hid': No such device
pi@pizero:~$ sudo modprobe -v g_hid
insmod /lib/modules/4.4.34+/kernel/drivers/usb/gadget/legacy/g_hid.ko
modprobe: ERROR: could not insert 'g_hid': No such device
pi@pizero:~$ ls -l /lib/modules/4.4.34+/kernel/drivers/usb/gadget/legacy/g_hid.k
-rw-r--r-- 1 root root 10928 11月 26 01:35 /lib/modules/4.4.34+/kernel/drivers/usb/gadget/legacy/g_hid.ko
```
- 残りの他のも含め宿題に……．

---

powered by [Marp - Markdown Presentation Writer](https://yhatt.github.io/marp/ "Marp - Markdown Presentation Writer")
Licence : [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/)