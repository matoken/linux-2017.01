<!-- $theme: default -->

<!-- page_number: true -->
<!-- $size: 4:3 -->
<!-- footer: @matoken -->


# USBを保護するUSBGuard

##### KenichiroMATOHARA([@matoken](https://twitter.com/matoken/))
##### http://matoken.org 
### 鹿児島Linux勉強会2017.01(2017-01-04)
### サンエールかごしま 中研修室1

---

# USBデバイスのセキュリティ

- マスストレージ経由でウィルス侵入
- 「ちょっと充電させて」とかスマホを繋いでそれ経由でウィルスやマルウェア感染とか
- [PoisonTap](http://hackaday.com/2016/11/16/poinsontap-makes-raspberry-pi-zero-exploit-locked-computers/ "PoisonTap Makes Raspberry Pi Zero Exploit Locked Computers | Hackaday")(PIZERO)や[USBdriveby](http://samy.pl/usbdriveby/ "Samy Kamkar - USBdriveby: exploiting USB in style")(Teensy or Arduino)でバックドア
- キーロガー
- USBメモリだけど中のFWが書き換わっていてマスストレージ+HIDデバイスになっていたり([Psychson](https://github.com/brandonlw/Psychson "brandonlw/Psychson: Phison 2251-03 (2303) Custom Firmware &amp; Existing Firmware Patches (BadUSB)")とか)
- モバイルバッテリーや充電ステーションに仕込んでスマホに感染とか
- AVRのV-USBやPICやでマイコンのUSB化も安く小さくお手軽


---

# USB以外の外部ポートを無効にする例

- BIOSやUEFIで無効にする
- モジュールを読み込まない(FireWireとThunderBoltの例)
```
blacklist firewire-core
blacklist thunderbolt
```
- セメントでポートを埋める…

---

# USBの場合

- 内蔵デバイスがUSB接続だったりして完全に無効にし辛い
（以下ではカメラやBluetoothが接続されている）

```
$ lsusb
Bus 004 Device 002: ID 8087:0024 Intel Corp. Integrated Rate Matching Hub
Bus 004 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
Bus 003 Device 005: ID 04f2:b217 Chicony Electronics Co., Ltd Lenovo Integrated Camera (0.3MP)
Bus 003 Device 003: ID 0a5c:21e6 Broadcom Corp. BCM20702 Bluetooth 4.0 [ThinkPad]
Bus 003 Device 002: ID 8087:0024 Intel Corp. Integrated Rate Matching Hub
Bus 003 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
```

---

# USBを保護したい

- 予想しないデバイスの接続を拒否したい
- いつの間にか繋げられたデバイス
- 見た目と違うプロトコルのデバイス
　 ：

---

# USBGuard

[![](./usbguard-banner-400.png)https://dkopecek.github.io/usbguard/](https://dkopecek.github.io/usbguard/)

- はじめにルールを作成
- ルールによりreject,block,allow出来る
- GUIで管理する`usbguard-applet-qt`を利用するとUSBデバイス接続時にポップアップで知らせてくれる&許可やブロックを指定できる

---

# USBGuardの導入

- 有名どころのLinuxディストリビューションにはパッケージがある
  - [Compilation And Instalation | USBGuard](https://dkopecek.github.io/usbguard/documentation/compilation.html "Compilation And Instalation | USBGuard")
- 以下はDebian stretch/Ubuntu 16.10での導入例
```
$ sudo apt install usbguard usbguard-applet-qt
```

---

# USBGuardの設定

- 一般ユーザから利用する場合は設定ファイルを編集してユーザかグループを追加してデーモンの再起動を行う
- 設定ファイルは`/etc/usbguard/usbguard-daemon.conf`
- ユーザの場合は`IPCAllowedUsers`，グループの場合は`IPCAllowedGroups`にスペース区切りで書く

---

```
diff --git a/usbguard/usbguard-daemon.conf b/usbguard/usbguard-daemon.conf
index 4a54ca0..7b3a165 100644
--- a/usbguard/usbguard-daemon.conf
+++ b/usbguard/usbguard-daemon.conf
@@ -65,7 +65,7 @@ PresentControllerPolicy=keep
#
# IPCAllowedUsers=username1 username2 ...
#
-IPCAllowedUsers=root
+IPCAllowedUsers=root user1 user2

#
# Groups allowed to use the IPC interface.
@@ -75,7 +75,7 @@ IPCAllowedUsers=root
#
# IPCAllowedGroups=groupname1 groupname2 ...
#
-IPCAllowedGroups=root
+IPCAllowedGroups=root users

#
# Generate device specific rules including the "via-port"
```

---

- 設定を反映するためにデーモンを再起動
```
$ sudo service usbguard restart
```

---

# USBBGuardのCUIでの利用例

- 既定値ではUSBGuardが起動した後に接続されたデバイスはblockされる

```
$ usbguard list-devices| tail -2
9: allow id 8087:0024 serial "" name "" hash "Zx7v0FMQEjScKSAFENAiobEs1OGPPB0YWR+yXDCVE04=" parent-hash "WwvSEwd+7257rAqUGLMQjffF7zyqygmmLeQTYnR9QzQ=" via-port "4-1" with-interface 09:00:00
11: block id 1004:631c serial "03a809c94b4befd4" name "LGE Android Phone" hash "P5dSK5xxK4R5QTRzd7KlD8Agf/+28pztL077j1oWqPI=" parent-hash "Zx7v0FMQEjScKSAFENAiobEs1OGPPB0YWR+yXDCVE04=" via-port "4-1.1" with-interface ff:ff:00
```

- blockされている11番目のデバイスを一時的に許可してみる
```
$ usbguard allow-device 11
$ usbguard list-devices| tail -1
11: allow id 1004:631c serial "03a809c94b4befd4" name "LGE Android Phone" hash "P5dSK5xxK4R5QTRzd7KlD8Agf/+28pztL077j1oWqPI=" parent-hash "Zx7v0FMQEjScKSAFENAiobEs1OGPPB0YWR+yXDCVE04=" via-port "4-1.1" with-interface ff:ff:00
```

---

# GUI(usbguard-applet-qt)の利用例

- 新しいデバイスを接続するとポップアップが表示される
- Allowを選ぶと接続
- タイムアウトを待つかBlockを選ぶと拒否
- Allow/Blockの前に`「Make the decision permanent」`をチェックしておくと設定が保存されて次回以降自動的に同じものが選択される
![](USBGuard-qt-01.jpg)

---

- 一旦設定したものも再設定が可能
- 「Permanently」にチェックを付けておくと設定が保存されて次回以降自動的に同じものが選択される
![](USBGuard-qt-02.png)

---

# USBGuardの初期デバイスを設定

- 既定値ではUSBGuardが起動するまでに接続されていたデバイスは全て許可される
- シャットダウン時などにPCに不正なデバイスが接続されていた場合は保護されない
- 前もって初期デバイスのみ許可するよう設定しておく
  - `usbguard`の`generate-policy`コマンドで設定を書き出して編集
  - ルールファイルを反映して動作確認

---

# ルールファイルを作成

- 初期デバイスを`allow`に，内蔵カメラ(不正なものではないが)とそれ以外のデバイスを`block`に

```
$ usbguard generate-policy > rules.conf
$ vi rules.conf
$ sudo cat /etc/usbguard/rules.conf
allow id 1d6b:0002 serial "0000:00:1a.0" name "EHCI Host Controller" hash "MwANH+QnAvclGgMNHjzanbOGkp3bPmwqoyAEZZ6QXTQ=" parent-hash "uvJm0y/N2iYeJgfKJsJqWKTJts/duhYZ7W2zzAYk7Y8=" with-interface 09:00:00
allow id 8087:0024 serial "" name "" hash "kv3v2+rnq9QvYI3/HbJ1EV9vdujZ0aVCQ/CGBYIkEB0=" parent-hash "MwANH+QnAvclGgMNHjzanbOGkp3bPmwqoyAEZZ6QXTQ=" via-port "3-1" with-interface 09:00:00
allow id 0a5c:21e6 serial "2016D8DA016E" name "BCM20702A0" hash "C4Os63DCRvIuWJYU/U+1PXrvWlXa2PmpRUQhp+C5eeE=" parent-hash "kv3v2+rnq9QvYI3/HbJ1EV9vdujZ0aVCQ/CGBYIkEB0=" with-interface { ff:01:01 ff:01:01 ff:01:01 ff:01:01 ff:01:01 ff:01:01 ff:01:01 ff:ff:ff fe:01:01 }
allow id 17ef:100a serial "" name "" hash "dMjTmGpj5dFGqH51kQpO/LVBQxE6JkwibVRJQkFCCuU=" parent-hash "kv3v2+rnq9QvYI3/HbJ1EV9vdujZ0aVCQ/CGBYIkEB0=" via-port "3-1.5" with-interface { 09:00:01 09:00:02 }
block id 04f2:b217 serial "" name "Integrated Camera" hash "BxFRAwzjkHO55cQGR8oMRm6bq+Ps2qQtU88jE1Uk5KE=" parent-hash "kv3v2+rnq9QvYI3/HbJ1EV9vdujZ0aVCQ/CGBYIkEB0=" via-port "3-1.6" with-interface { 0e:01:00 0e:02:00 0e:02:00 0e:02:00 0e:02:00 0e:02:00 0e:02:00 0e:02:00 0e:02:00 }
allow id 1d6b:0002 serial "0000:05:00.0" name "xHCI Host Controller" hash "IV7wk04gfQJink/IY4TiGVdcmTzuc09WcSe6k57kWrs=" parent-hash "3TIXKJ1dp4XFV6VxxWU11xbI0yLS0VmRZIaxdsLZDx4=" with-interface 09:00:00
allow id 1d6b:0003 serial "0000:05:00.0" name "xHCI Host Controller" hash "VlZK5oVuQQAlBH76Ekgc+KaZZDL0BAsF9tEf1ynb154=" parent-hash "3TIXKJ1dp4XFV6VxxWU11xbI0yLS0VmRZIaxdsLZDx4=" with-interface 09:00:00
allow id 1d6b:0002 serial "0000:00:1d.0" name "EHCI Host Controller" hash "WwvSEwd+7257rAqUGLMQjffF7zyqygmmLeQTYnR9QzQ=" parent-hash "FjkaT8Rp/Bh++KC4RQhk++hWack2wTDa1a1G5yXqYys=" with-interface 09:00:00
allow id 8087:0024 serial "" name "" hash "Zx7v0FMQEjScKSAFENAiobEs1OGPPB0YWR+yXDCVE04=" parent-hash "WwvSEwd+7257rAqUGLMQjffF7zyqygmmLeQTYnR9QzQ=" via-port "4-1" with-interface 09:00:00
block
```

---

- 設定ファイルをコピーしてデーモンを再起動して反映．


```
$ sudo install -m 0600 -o root -g root rules.conf /etc/usbguard/rules.conf
$ sudo systemctl restart usbguard
```

---

# ルールについて

- 細かいルール設定が可能
  - [Rule Language | USBGuard](https://dkopecek.github.io/usbguard/documentation/rule-language.html "Rule Language | USBGuard")
- USBメモリだけを許可する
- Yubikeyを指定ポートに刺したときだけ許可する
- randomに許可する!?

---

# Tips

- Blockしても給電はされるのでスマホ充電可能
- GUIアプリのリストが更新されないときがある
  - [Reset]ボタンを押してみる
- GUIアプリのリストが空欄になるときがある
  - GUIアプリ再起動
  - 駄目ならdaemonも再起動
```
$ sudo service usbguard restart
$ sudo service usbguard-dbus restart
```

---

# USBGuardも標準に?

- PCにもFirewallを設定するようにUSBGuardも標準に?
- NotePCだけでなくデスクトップやサーバにもUSBGuardを入れると良さそう
- サーバなどの場合は`block`ではなく`reject`の方が良さそう

---

# 充電に限れば

- [充電専用ケーブル](http://amzn.to/2iuxU0J)や充電専用アダプタというものがある
- 外でスマホ充電時におすすめ
![](smartCharge.jpg)
  [PortaPow Fast Charge + Data Block USB Adaptor - PortaPow](http://www.portablepowersupplies.co.uk/portapow-fast-charge-data-block-usb-adaptor/ "PortaPow Fast Charge + Data Block USB Adaptor - PortaPow")

---

![](airport-carge.JPG)
- こういうところは[microUSB Bメス-USB Aメス](http://amzn.to/2iyOyuc)-[充電専用ケーブル](http://amzn.to/2iuxU0J)経由とかしないと駄目そう(使わないのが一番)

---

# 余録)USB Killer

![](usb-killer.jpg)
[USB Killer V2.0 - Available now - buy online! - USB Killer](https://www.usbkill.com/ "USB Killer V2.0 - Available now - buy online! - USB Killer")
- USBメモリのような形状のデバイス
- PCに接続すると昇圧して高圧電流を作りPCに流して破壊する
- **USBGuardでは回避できない**
- 回避方法はセメントで埋める……?

---

powered by [Marp - Markdown Presentation Writer](https://yhatt.github.io/marp/ "Marp - Markdown Presentation Writer")
Licence : [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/)